#include "stdafx.h"
#include "Chess++.h"
#include "Defines.h"
#include "ChessOracle.h"
#include "Utilities.h"
#include <chrono>
#include <intrin.h>


using namespace std::chrono;

U64 Utilities::Timestamp()
{
	//return std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now() - Program::ProgramStartTime).count();
	return 0;
}

std::string Utilities::LAN(U16 move)
{
	std::string lan = SquareName(ChessOracle::StartSquare(move)) + SquareName(ChessOracle::EndSquare(move));
	if (ChessOracle::IsPromotion(move))
		lan += Utilities::LetterFromPiece(ChessOracle::PromotionType(move), false);
	return lan;
}

U64 Utilities::RowsR(U64 bitBoard, U8 shift)
{
	return ((bitBoard & 0x00000000000000FF) << shift) & 0x00000000000000FF
		| ((bitBoard & 0x000000000000FF00) << shift) & 0x000000000000FF00
		| ((bitBoard & 0x0000000000FF0000) << shift) & 0x0000000000FF0000
		| ((bitBoard & 0x00000000FF000000) << shift) & 0x00000000FF000000
		| ((bitBoard & 0x000000FF00000000) << shift) & 0x000000FF00000000
		| ((bitBoard & 0x0000FF0000000000) << shift) & 0x0000FF0000000000
		| ((bitBoard & 0x00FF000000000000) << shift) & 0x00FF000000000000
		| ((bitBoard & 0xFF00000000000000) << shift);
}

U64 Utilities::RowsL(U64 bitBoard, U8 shift)
{
	return ((bitBoard & 0x00000000000000FF) >> shift)
		| ((bitBoard & 0x000000000000FF00) >> shift) & 0x000000000000FF00
		| ((bitBoard & 0x0000000000FF0000) >> shift) & 0x0000000000FF0000
		| ((bitBoard & 0x00000000FF000000) >> shift) & 0x00000000FF000000
		| ((bitBoard & 0x000000FF00000000) >> shift) & 0x000000FF00000000
		| ((bitBoard & 0x0000FF0000000000) >> shift) & 0x0000FF0000000000
		| ((bitBoard & 0x00FF000000000000) >> shift) & 0x00FF000000000000
		| ((bitBoard & 0xFF00000000000000) >> shift) & 0xFF00000000000000;
}

U64 Utilities::ShiftOne(U64 bitBoard, U8 dir)
{
	switch (dir)
	{
	case N:
		return bitBoard << 8;
	case NE:
		return (0x7F7F7F7F7F7F7F7F & bitBoard) << 9;
	case E:
		return (0x7F7F7F7F7F7F7F7F & bitBoard) << 1;
	case SE:
		return (0x7F7F7F7F7F7F7F7F & bitBoard) >> 7;
	case S:
		return bitBoard >> 8;
	case SW:
		return (0xFEFEFEFEFEFEFEFE & bitBoard) >> 9;
	case W:
		return (0xFEFEFEFEFEFEFEFE & bitBoard) >> 1;
	case NW:
		return (0xFEFEFEFEFEFEFEFE & bitBoard) << 7;
	default:
		return bitBoard;
	}
}

U64 Utilities::HalfShuffle(U64 x)
{
	x = ((x & 0xFF00FF00) << 16) | (x & 0x00FF00FF);
	x = ((x << 4) | x) & 0x0F0F0F0F0F0F0F0F;
	x = ((x << 2) | x) & 0x3333333333333333;
	x = ((x << 1) | x) & 0x5555555555555555;
	return x;
}

__int8 Utilities::MoverPawnDelta(GameState state) 
{
	return 8 - (state.Meta & 1) * 16; 
}

/* From chessprogramming.wikispaces.com */
U64 Utilities::PseudoRotate45clockwise(U64 x) {
	const U64 k1 = 0xAAAAAAAAAAAAAAAA;
	const U64 k2 = 0xCCCCCCCCCCCCCCCC;
	const U64 k4 = 0xF0F0F0F0F0F0F0F0;
	x ^= k1 & (x ^ _rotr64(x, 8));
	x ^= k2 & (x ^ _rotr64(x, 16));
	x ^= k4 & (x ^ _rotr64(x, 32));
	return x;
}

/* From chessprogramming.wikispaces.com */
U64 Utilities::PseudoRotate45antiClockwise(U64 x) {
	const U64 k1 = 0x5555555555555555;
	const U64 k2 = 0x3333333333333333;
	const U64 k4 = 0x0F0F0F0F0F0F0F0F;
	x ^= k1 & (x ^ _rotr64(x, 8));
	x ^= k2 & (x ^ _rotr64(x, 16));
	x ^= k4 & (x ^ _rotr64(x, 32));
	return x;
}

U8 Utilities::log2(const U64 x)
{
	unsigned long log2 = 0;
	if (_BitScanReverse64(&log2, x) == 0)
		return 0xFF;
	return (U8)log2;
}

U8 Utilities::trailing0s(const U64& x)
{
	unsigned long count = 0;
	_BitScanForward64(&count, x);
	return count;
}

U16 Utilities::PgnToMove(std::string pgn, GameState state)
{
	bool whiteMove = (state.Meta & 1) == 0;
	bool checkmate	 = pgn[pgn.length() - 1] == '#';
	bool check		 = pgn[pgn.length() - 1] == '+';
	bool kingCastle  = !strcmp(pgn.c_str(), "O-O");
	bool queenCastle = !strcmp(pgn.c_str(), "O-O-O");
	if (checkmate || check)
		pgn = pgn.substr(0, pgn.length() - 1);
	bool promotion   = pgn[pgn.length() - 2] == '=';

	if (kingCastle  && whiteMove)  return 0x106;
	if (kingCastle  && !whiteMove) return 0xF3E;
	if (queenCastle && whiteMove)  return 0x102;
	if (queenCastle && !whiteMove) return 0xF3A;

	U16 move = 0;

	if (promotion)
	{
		move |= (Utilities::PieceFromLetter(pgn[pgn.length() - 1])  - 2) << 14;
		pgn = pgn.substr(0, pgn.length() - 2);
	}

	U8 endRank = pgn[pgn.length() - 1] - '0' - 1;
	U8 endFile = pgn[pgn.length() - 2] - 'a';
	U8 endSquare = endRank * 8 + endFile;
	pgn = pgn.substr(0, pgn.length() - 2);
	move |= endSquare;

	bool noncapturingPawnMove = pgn.length() == 0;
	if (noncapturingPawnMove)
	{
		U8 pawnMoveDirection = whiteMove ? 8 : -8;
		U8 startSquare = ChessOracle::GetPieceOnSquare(state, endSquare - pawnMoveDirection) == (Piece::bpawn || Piece::wpawn) ?
			endSquare - pawnMoveDirection :
			endSquare - 2 * pawnMoveDirection;

		return move | (startSquare << 6);
	}

	bool capture = pgn[pgn.length() - 1] == 'x';
	if (capture)
		pgn = pgn.substr(0, pgn.length() - 1);
	bool capturingPawnMove = capture && 'a' <= pgn[0] && pgn[0] <= 'h';
	if (capturingPawnMove)
	{
		U8 direction = whiteMove ? 8 : -8;
		U8 startSquare = (pgn[0] - 'a') + 8*(endSquare/8) - direction;
		return move | (startSquare << 6);
	}
	bool ambiguousStartFileOnly = pgn.length() == 2;
	bool ambiguousStartRankAndFile = pgn.length() == 3;
	
	Piece mover = PieceFromLetter(pgn[0]);
	U64 moverStartPossibilities = ambiguousStartFileOnly ?
		ChessOracle::CardinalSquaresVParallel_Mask(1 << (U8)(pgn[1] - 'a')) :
		ambiguousStartRankAndFile ?
		(pgn[1] - 'a') + 8 * (pgn[2] - '0') - whiteMove ? 8 : -8 :
		0x0;
	
	moverStartPossibilities |=
		mover == Piece::knight ? ChessOracle::KnightReachSquaresParallel_Mask(1 << endSquare) :
		mover == Piece::king ? ChessOracle::SqsAndSurroundingSqsParallel_Mask(1 << endSquare) :
		0x0;

	U64 endSqrMask = ((U64)1) << endSquare;
	if (mover == Piece::bishop || mover == Piece::queen)
	{
		moverStartPossibilities |=
			ChessOracle::DROccRay(endSqrMask, state) |
			ChessOracle::DLOccRay(endSqrMask, state);
	}

	if (mover == Piece::rook || mover == Piece::queen)
	{
		moverStartPossibilities |=
			ChessOracle::NSOccRay(endSqrMask, state) |
			ChessOracle::EWOccRay(endSqrMask, state);
	}

	//U64 startSquareMask = ChessOracle::SquaresContainingPiece(state, moverStartPossibilities, mover);
	U8 startSquare = log2(moverStartPossibilities/*startSquareMask*/);

	return move | (startSquare << 6);
}

std::string Utilities::MoveToPgn(U16 move, GameState state)
{
	bool whiteMove = (state.Meta & 1) == 0;
	GameState after = ChessOracle::GetMoveResultWithPromotion(state, move);
	std::string pgn = SquareName((move >> 6) & 0x3F).append(SquareName(move & 0x3F));
	return pgn;
}

std::string Utilities::PositionToFEN(GameState position)
{
	std::string fen = "";
	int blanks = 0;
	for (int i = 63; i >= 0; i--)
	{
		if (i > 0 && i % 8 == 0)
		{
			if (blanks > 0)
				fen += std::to_string(blanks);
			blanks = 0;
			fen += '/';
		}

		Piece p = ChessOracle::GetPieceOnSquare(position, i);
		char pc = LetterFromPiece(p, ((position.State0 >> i) & 1) == 0);
		if (pc == '.')
		{
			blanks++;
			continue;
		}

		if (blanks > 0)
			fen += std::to_string(blanks);
		blanks = 0;
		fen += pc;
	}

	fen += ' ';
	fen += (((position.Meta & 1) == 0) ? 'w' : 'b');
	fen += ' ';
	if (ChessOracle::ColorMayCastleK(position, 0))
		fen += 'K';
	if (ChessOracle::ColorMayCastleK(position, 0))
		fen += 'Q';
	if (ChessOracle::ColorMayCastleK(position, 1))
		fen += 'k';
	if (ChessOracle::ColorMayCastleK(position, 1))
		fen += 'q';
	if (ChessOracle::NobodyMayCastle(position))
		fen += '-';

	fen += ' ';

	if (ChessOracle::EnPassantTarget(position) > 0)
		fen += std::to_string(Utilities::trailing0s(ChessOracle::EnPassantTarget(position)));
	else
		fen += '-';

	fen += ' ' + std::to_string(2 * ChessOracle::MovesSinceProgress(position) + (position.Meta & 1));

	fen += " 0"; //TODO Not tracking fullmove number.

	return fen;
}

std::string Utilities::SquareName(U8 square)
{
	char file = (square & 0x7) + 'a';
	char rank = ((square >> 3) & 0x7) + '1';
	std::string out = "";
	out += file;
	out += rank;
	return out;
}

GameState Utilities::MarkSquares(GameState state, U64 squares)
{
	state.State1 |= squares;
	state.State2 |= squares;
	state.State3 |= squares;
	return state;
}

void Utilities::PrintState(GameState state, bool whiteAtBottom, bool justLog)
{
	std::string s = "";
	for (int row_i = 7; row_i >= 0; row_i--)
	{
		int row = whiteAtBottom ? row_i : 7 - row_i;
		for (int col_i = 0; col_i < 8; col_i++)
		{
			int col = whiteAtBottom ? col_i : 7 - col_i;
			Piece piece = ChessOracle::GetPieceOnSquare(state, 8 * row + col);
			bool white = ((state.State0 >> (col + 8 * row)) & 1) == 0;
			char cPiece =
				piece == Piece::blank	? cPiece = '.' :
				piece == Piece::wpawn 
			||	piece == Piece::bpawn	? cPiece = 'P' :
				piece == Piece::knight	? cPiece = 'N' :
				piece == Piece::bishop	? cPiece = 'B' :
				piece == Piece::rook	? cPiece = 'R' :
				piece == Piece::queen	? cPiece = 'Q' :
				piece == Piece::king	? cPiece = 'K' :
				'x';
			if (!white)
				cPiece = tolower(cPiece);
			s += cPiece;
		}
		s += '\n';
	}
	s += '\n';
	s += (state.Meta & 1) == 0 ? "White to move." : "Black to move.";
	s += '\n';

	if (!justLog)
		printf(s.c_str());
	Program::Log(s);
}

void Utilities::PrintBitboard(U64 board)
{
	for (int bit = 0; bit < 64; bit++)
	{
		if (bit % 8 == 0) printf("\n");
		printf((board & (1ULL << bit)) > 0 ? "1" : "0");
	}
}

void Utilities::PrintLegalMoves(GameState state)
{
	U64 butterfly[64];
	U64 movers = 0;
	bool inCheck = false;
	ChessOracle::GetLegalMoves(Game::CurrentGame.GetState(), butterfly, inCheck, movers);
	for (U16 square = 0; square < 64; square++)
	{
		U64& destinations = butterfly[square];
		for (; destinations > 0; destinations &= destinations - 1)
		{
			U16 move = (square << 6) | Utilities::trailing0s(destinations);
			Program::Output(Utilities::LAN(move));
		}
	}
}

Piece Utilities::PieceFromLetter(char letter)
{
	switch (letter)
	{
	case 'K':
	case 'k':
		return Piece::king;
	case 'N':
	case 'n':
		return Piece::knight;
	case 'B':
	case 'b':
		return Piece::bishop;
	case 'R':
	case 'r':
		return Piece::rook;
	case 'Q':
	case 'q':
		return Piece::queen;
	case 'P':
		return Piece::wpawn;
	case 'p':
		return Piece::bpawn;
	default:
		return Piece::blank;
	}
}

char Utilities::LetterFromPiece(Piece piece, bool white)
{
	char p = 
		piece == blank ? '.'
	:	piece == wpawn	? 'P'
	:	piece == bpawn	? 'p'
	:	piece == knight	? 'N'
	:	piece == bishop	? 'B'
	:	piece == rook	? 'R'
	:	piece == queen	? 'Q'
	:	piece == king	? 'K'
	:	'X';

	return white ? p : tolower(p);
}