#include "stdafx.h"
#include "Chess++.h"
#include "ChessOracle.h"
#include "AI0.h"
#include "Utilities.h"
#include "InterfaceLayer.h"
#include "Profiler.h"
#include <algorithm>
#include <chrono>

const std::string AI0::ENGINE_NAME = "Chess++ AI0";
const std::string AI0::ENGINE_AUTHOR = "Nicholas Gyde";

AI0::AI0()
{
	Searching = ATOMIC_VAR_INIT(false);
	SearchStartTime = 0;
	NodesSearched = 0;
	BestLine = new U16[64];
	BestLineScore = 0;
	BestLineDepth = 0;
	DepthLimit = 1;
	CurrentIterationDepthLimit = 0;
	SearchThread = nullptr;
	InfoThread = nullptr;
}


AI0::~AI0()
{
}

void AI0::Ponder(Game game, U8 depth, U64 timeLimit)
{
	Program::Log("Pondering.");
	DepthLimit = depth == 255 ? DEFAULT_SEARCH_DEPTH_PLY : depth;
	TimeLimit = timeLimit;
	SearchThread = new std::thread(&AI0::Search, this, game.GetState());
	InfoThread = new std::thread(&AI0::SendInfo, this, game.GetState());
	SearchThread->detach();
	InfoThread->detach();
}

void AI0::StopPondering()
{
	Searching.store(false);
}

void AI0::SendInfo(GameState& state)
{
	U8 argFlags = 0;
	U64 nodesSearchedRecord = 0;
	U16 recordBestScore = 0;
	std::vector<U16> vec;

	while(Searching.load() && BestLineDepth == 0)
	{
		std::this_thread::yield();
	}

	while (Searching.load())
	{
		std::this_thread::sleep_for(std::chrono::seconds(1));
		bool mateFound = BestLineScore >= MATE_IN(1000) || BestLineScore <= -MATE_IN(1000);
		argFlags = mateFound ? 0xEF : 0xDF;
		nodesSearchedRecord = Utilities::Max(NodesSearched, nodesSearchedRecord);
		if (BestLineScore >= recordBestScore && BestLine != nullptr && BestLineDepth > 0)
		{
			recordBestScore = BestLineScore;
			vec = std::vector<U16>(BestLine, BestLine + BestLineDepth);
		}
		Program::InterfaceLayer->SendInfo(BestLineDepth, Utilities::Timestamp() - SearchStartTime, nodesSearchedRecord, vec, state.Meta & 1 > 0 ? -BestLineScore : BestLineScore, MATE_IN(0) - labs(BestLineScore), argFlags);
	}
	Program::InterfaceLayer->AnnounceMove(BestLine[0]);
}

U8 AI0::DefaultFirstIterationDepth() const
{
	return 7; //TODO some function of remaining time.
}

void AI0::Search(GameState state)
{
	Searching.store(true);
	BestLineDepth = 0;
	BestLineScore = 0;
	SearchStartTime = Utilities::Timestamp();
	CurrentIterationDepthLimit = Utilities::Min(DefaultFirstIterationDepth(), DepthLimit);
	U16** continuations = new U16*[DepthLimit + 1];
	for (U8 depth = 1; depth <= DepthLimit; depth++)
		continuations[depth] = new U16[depth];

	//Iterative deepening
	while (Searching.load() && 0 < CurrentIterationDepthLimit && CurrentIterationDepthLimit <= DepthLimit && Utilities::Timestamp() - SearchStartTime < TimeLimit)
	{
		NodesSearched = 0;
		__int32 bestLineScoreThisIteration = Negamax(state, CurrentIterationDepthLimit, -MATE_IN(0), MATE_IN(0), continuations);
		for (int i = 0; i <= CurrentIterationDepthLimit; i++)
			BestLine[i] = continuations[CurrentIterationDepthLimit][i];
		BestLineScore = bestLineScoreThisIteration;
		BestLineDepth = CurrentIterationDepthLimit;
		CurrentIterationDepthLimit++;
	}

	for (U8 depth = 1; depth <= DepthLimit; depth++)
		delete[] continuations[depth];
	delete[] continuations;
	Searching.store(false);
}

__int32 AI0::Negamax(GameState state, const U8 depth, __int32 a, const __int32 b, U16** continuations)
{
	if (depth == 0) 
	{
		NodesSearched++;
		return (((state.Meta & 1) > 0) ? -1 : 1) * HeuristicValue(state);
	}

	bool inCheck = false;
	U64 movers = 0;
	U64 butterfly[64];
	ChessOracle::GetLegalMoves(state, butterfly, inCheck, movers);

	if (movers == 0)
		return inCheck ? -MATE_IN((CurrentIterationDepthLimit - depth) / 2) : 0;

	U64 best8Moves = 0; //GuessBest8Moves(state, butterfly, movers); TODO Too slow

	__int32 best = -MATE_IN(0);
	__int32 negaresult = 0;
	U8 destination;
	U16 move;
	while (movers > 0)
	{
		//Decide next move to search.
		U64 nextMovers = movers;

		if (best8Moves > 0) //TODO Misses best move if it's mover 0 to dest 0.
		{
			U8 mover_i = best8Moves & 0xE0;
			for (; mover_i < 0xFF; mover_i--)
				nextMovers &= nextMovers - 1;
		}
		
		const U8 moverSqNum = Utilities::trailing0s(nextMovers);
		U64 destinations = butterfly[moverSqNum];

		if (best8Moves > 0)
		{
			U8 dest_i = best8Moves & 0x1F;
			for (; dest_i < 0xFF; dest_i--)
				destinations &= destinations - 1;

#pragma warning (disable : 4146)
			destinations &= -destinations;
#pragma warning (restore : 4146)
			butterfly[moverSqNum] &= ~destinations; //Don't search this move again later.
			best8Moves >>= 8;
		}
		else
			movers &= movers - 1;

		//Search.
		for (; destinations > 0; destinations &= destinations - 1)
		{
			destination = Utilities::trailing0s(destinations);
			move = (moverSqNum << 6) | destination;
			ChessOracle::SetPromotionBit(move, state);

			do
			{
				negaresult = -Negamax(ChessOracle::GetMoveResultWithPromotion(state, move), depth - 1, -b, -a, continuations);
				if (negaresult > best)
				{
					best = negaresult;
					for (U8 h = depth - 1; h > 0 && depth > 1; h--)
						continuations[depth][h] = continuations[depth - 1][h - 1];
					continuations[depth][0] = move;
				}
				NodesSearched++;

				a = Utilities::Max(a, negaresult);
				if (a >= b)
					return best;

				move += 0x4000;
			} while (ChessOracle::IsPromotion(move) && (move & 0xC000) > 0);
		}
	}
	return best;
}

__int32 AI0::HeuristicValue(GameState& state)
{
	__int32 score = 0;
	const U64 white = ~state.State0;

	score += 100 * (Utilities::HammingWeight(ChessOracle::WPawns(state)) - Utilities::HammingWeight(ChessOracle::BPawns(state)));
	score += 300 * 
		(
			Utilities::HammingWeight(ChessOracle::Knights(state) & white) + Utilities::HammingWeight(ChessOracle::Bishops(state) & white)
		-	Utilities::HammingWeight(ChessOracle::Knights(state) & ~white) - Utilities::HammingWeight(ChessOracle::Bishops(state) & ~white)
		);
	score += 500 * (Utilities::HammingWeight(ChessOracle::Rooks(state) & white) - Utilities::HammingWeight(ChessOracle::Rooks(state) & ~white));
	score += 900 * (Utilities::HammingWeight(ChessOracle::Queens(state) & white) - Utilities::HammingWeight(ChessOracle::Queens(state) & ~white));

	return score;
}


/*
*Each move is
* 3 : 5
* mover index : destination index
* Better moves returned farther from lo byte.
*/
U64 AI0::GuessBest8Moves(GameState& state, U64* butterfly, U64 movers)
{
	//
	U64 ordering = 0; //array of 8 moves
	U64 scores = 0; //array of 8 scores (not measured in cp here)

	U8 destination;
	U8 mover_i = 0;
	U8 dest_i = 0;
	U8 curScore = 0;
	for (; movers > 0; movers &= movers - 1, mover_i++)
	{
		U64 destinations = butterfly[Utilities::trailing0s(movers)];
		for (; destinations > 0; destinations &= destinations - 1, dest_i++)
		{
			const U8 destination = Utilities::trailing0s(destinations);
			//Calculate curScore
			curScore = 0; //TODO Ordering scores.
			
			//Compress move.
			const U8 curMove8Bit = (mover_i << 5) | dest_i;

			//Binary search for insertion point in scores and moves arrays.
			if (curScore > Utilities::NthByteFromLo(scores, 3))
				if (curScore > Utilities::NthByteFromLo(scores, 5))
					if (curScore > Utilities::NthByteFromLo(scores, 6))
						if (curScore > Utilities::NthByteFromLo(scores, 7))
							InsertAfterNth(ordering, scores, curMove8Bit, curScore, 7);
						else
							InsertAfterNth(ordering, scores, curMove8Bit, curScore, 6);
					else
						InsertAfterNth(ordering, scores, curMove8Bit, curScore, 5);
				else
					if (curScore > Utilities::NthByteFromLo(scores, 4))
						InsertAfterNth(ordering, scores, curMove8Bit, curScore, 4);
					else
						InsertAfterNth(ordering, scores, curMove8Bit, curScore, 3);
			else
				if (curScore > Utilities::NthByteFromLo(scores, 1))
					if (curScore > Utilities::NthByteFromLo(scores, 2))
						InsertAfterNth(ordering, scores, curMove8Bit, curScore, 2);
					else
						InsertAfterNth(ordering, scores, curMove8Bit, curScore, 1);
				else if (curScore > Utilities::NthByteFromLo(scores, 0))
					InsertAfterNth(ordering, scores, curMove8Bit, curScore, 0);
		}
	}

	return ordering;
}