#pragma once
#include "Defines.h"
#include <vector>

class InterfaceLayer
{
public:
	virtual void ProcessInput(const char* input) = 0;
	virtual void SendInfo(U8 depth, U64 time_ms, U64 nodes, std::vector<U16> pv, __int32 score_cp, __int8 mateIn, U8 ARG_FLAGS) = 0;
	virtual void AnnounceMove(U16 move) = 0;
};