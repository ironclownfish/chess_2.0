#pragma once
#include <chrono>
#include "Defines.h"

struct GameState;
class InterfaceLayer;
class AI;
class Profiler;

class Program
{

public:

	enum OptionFlag
	{
		Debug = 1
	};

	static InterfaceLayer* InterfaceLayer;
	static AI* Engine;
	static Profiler* Profiler;
	static U64 OptionFlags;
	static std::chrono::steady_clock::time_point ProgramStartTime;

	static void ExecuteCommand(const char* arg);

	static void PrintBestMoves(GameState state);
	static void CycleLegalMoves(GameState state);
	static void ProfileMoveGeneration(GameState state);

	static void Output(std::string s);
	static void Log(std::string s);
	static void Quit();

};