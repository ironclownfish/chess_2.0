#pragma once
#include "AI.h"
#include <thread>
#include <atomic>

class AI0 : public AI
{
private:
	static const U8 DEFAULT_SEARCH_DEPTH_PLY = 7;


public:

	AI0();
	~AI0();

	virtual void Ponder(Game game, U8 depth = 255, U64 timeLimit = MAXU64) override;
	virtual void StopPondering() override;

private:

	std::thread* SearchThread;
	std::thread* InfoThread;

	std::atomic<bool> Searching;
	U64 SearchStartTime;
	U64 TimeLimit;

	U64 NodesSearched;
	U16* BestLine;
	__int32 BestLineScore;
	U8 BestLineDepth;
	U8 CurrentIterationDepthLimit;
	U8 DepthLimit;

	void Search(GameState state);
	U8 DefaultFirstIterationDepth() const;
	void SendInfo(GameState& state);
	__int32 Negamax(GameState state, const U8 depth, __int32 a, __int32 b, U16** continuations);
	__int32 HeuristicValue(GameState& state);
	U64 GuessBest8Moves(GameState& state, U64* butterfly, U64 movers);

	//Helper for move ordering.
	inline void InsertAfterNth(U64& moves, U64& scores, U8 move, U8 score, U8 nthFromLo)
	{
		nthFromLo <<= 3; //Change units to bits.
		const U64 maskAboveInsert = (0xFFFFFFFFFFFFFF00 << nthFromLo);
		moves = (moves & maskAboveInsert) | (move << nthFromLo) | ((moves & ~maskAboveInsert) >> 8);
		scores = (scores & maskAboveInsert) | (scores << nthFromLo) | ((scores & ~maskAboveInsert) >> 8);
	}
};

