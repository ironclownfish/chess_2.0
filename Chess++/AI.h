#pragma once
#include "Defines.h"

class Game;

class AI
{
public:
	
	static const std::string ENGINE_NAME;
	static const std::string ENGINE_AUTHOR;

	virtual void Ponder(Game game, U8 depth = 255, U64 timeLimit = MAXU64) {}
	virtual void StopPondering() {}
};

