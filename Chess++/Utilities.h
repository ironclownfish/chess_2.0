#include "Defines.h"
#include <string>
#include <nmmintrin.h>

struct GameState;
enum Piece;

namespace Utilities
{	
	U64 Timestamp();
	std::string LAN(U16 move);
	U64 RowsL(U64 bitBoard, U8 shift);
	U64 RowsR(U64 bitBoard, U8 shift);
	U64 ShiftOne(U64 bitBoard, U8 dir);
	U64 HalfShuffle(U64 x);
	inline __int32 HammingWeight(U64 x){return _mm_popcnt_u64(x);}
	inline U8 SparseHW(U64 x) { U8 count = 0; for (; x; x &= x - 1) { ++count; } return count; }
	inline U64 Fill(U8 lsb) { return 0ULL - (lsb & 1); }
	inline __int8 MoverPawnDelta(GameState state);

	U64 PseudoRotate45clockwise(U64 x);
	U64 PseudoRotate45antiClockwise(U64 x);
	inline U64 byteswap(const U64& x) { return _byteswap_uint64(x); }
	inline U64 MirrorH(U64 x)
	{
		x = ((x >> 1) & 0X5555555555555555) | ((x << 1) & 0xAAAAAAAAAAAAAAAA);
		x = ((x >> 2) & 0x3333333333333333) | ((x << 2) & 0xCCCCCCCCCCCCCCCC);
		x = ((x >> 4) & 0x0F0F0F0F0F0F0F0F) | ((x << 4) & 0xF0F0F0F0F0F0F0F0);
		return x;
	}

	U8 log2(const U64 x);
	U8 trailing0s(const U64& x);
	inline U64 Max(U64 a, U64 b) {return a > b ? a : b;}
	inline __int32 Max(__int32 a, __int32 b) { return a > b ? a : b; }
	inline U64 Min(U64 a, U64 b) {return a < b ? a : b;}
	inline __int32 Min(__int32 a, __int32 b) { return a < b ? a : b; }
	inline U64 AbsDiff(U64 a, U64 b) { return a > b ? a - b : b - a; }
	inline U8 NthByteFromLo(U64 bytes, U8 n) { return ((bytes >> (n << 3)) & 0xFF); }

	U16 PgnToMove(std::string pgn, GameState state);
	std::string MoveToPgn(U16 move, GameState state);
	std::string PositionToFEN(GameState position);
	inline std::string SquareName(U8 square);
	GameState MarkSquares(GameState state, U64 squares);
	void PrintState(GameState state, bool whiteAtBottom, bool justLog = false);
	void PrintBitboard(U64 board);
	void PrintLegalMoves(GameState state);
	Piece PieceFromLetter(char letter);
	char LetterFromPiece(Piece piece, bool white);
};

