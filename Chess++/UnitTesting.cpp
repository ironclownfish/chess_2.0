#include "stdafx.h"
#include "ChessOracle.h"
#include "Chess++.h";
#include "Utilities.h"
#include "UnitTesting.h"


bool UnitTesting::RunTests()
{
	bool passedAllSoFar = true;
	passedAllSoFar &= RunIsolatedPieceTests();
	return passedAllSoFar;
}

bool UnitTesting::RunIsolatedPieceTests()
{
	bool passedSoFar = true;
	for (int sq = 0; sq < 64; sq++)
	for (auto piece : { Piece::bpawn, Piece::wpawn, Piece::knight, Piece::bishop, Piece::rook, Piece::queen, Piece::king })
	{
		GameState state = ChessOracle::BlankPosition;
		PlacePiece(state, piece, 1ULL << sq);
		U64 butterfly[64];
		passedSoFar &= VerifyLegalMoves(state, butterfly);
		state.Meta ^= 1;
		passedSoFar &= VerifyLegalMoves(state, butterfly);
	}

	return passedSoFar;
}

bool UnitTesting::VerifyLegalMoves(GameState& state, U64* butterfly)
{
	bool legalSoFar = true;

	int destination;
	U16 move;
	for (int start = 0; start < 64; start++)
	{
		U64 destinations = butterfly[start];
		for (; destinations > 0; destinations &= destinations - 1)
		{
			bool legal = true;
			destination = Utilities::trailing0s(destinations);
			move = (start << 6) | destination;

			//Destination is a king.
			legal &= (ChessOracle::Kings(state) & (1ULL << destination)) == 0;

			if (!legal)
			{
				Program::Log("------------------\nIllegal move!\nKing capture.\nPosition: " + Utilities::PositionToFEN(state) + "\nMove: " + Utilities::LAN(move) + "\n");
				Utilities::PrintState(state, true, true);
				Program::Log("------------------");
			}

			//Verify
			GameState after = ChessOracle::GetMoveResultWithPromotion(state, move);

			//In check after move.
			after.Meta ^= 1; //Pretend same person's turn again.
			bool check = false;
			U64 butterfly2[65]; //Add extra square to avoid overflow from missing king.
			U64 movers = 0;
			ChessOracle::GetLegalMoves(after, butterfly2, check, movers);
			legal = legal && !check;
			after.Meta ^= 1;

			if (!legal)
			{
				Program::Log("------------------\nIllegal move!\nMoved into check.\nPosition: " + Utilities::PositionToFEN(state) + "\nMove: " + Utilities::LAN(move) + "\n");
				Utilities::PrintState(state, true, true);
				Program::Log("------------------");
			}

			legalSoFar = legalSoFar && legal;
			if (!legalSoFar)
				break;

			//Moves onto own color piece.
			bool selfCapture = ((state.State0 >> destination) & 1) == state.Meta & 1;

			Piece piece = ChessOracle::GetPieceOnSquare(state, start);
			bool validEnPassantDest = ((state.State0 >> destination) & 1) == 1;
			int absDiff = Utilities::AbsDiff(start, destination);
			switch (piece)
			{
			case blank:
				selfCapture = false; //Get rid of en-passant square case.
				break;
			case wpawn:
				legalSoFar &= legalSoFar &&
					(((ChessOracle::GetPieceOnSquare(state, destination) != Piece::blank || validEnPassantDest) && (destination == start + 7 || destination == start + 9))
						|| (ChessOracle::GetPieceOnSquare(state, destination) == Piece::blank && (destination == start + 8 || (destination == start + 16 && start / 8 == 1))));
				break;
			case bpawn:
				legalSoFar &= legalSoFar &&
					(((ChessOracle::GetPieceOnSquare(state, destination) != Piece::blank || validEnPassantDest) && (destination == start - 7 || destination == start - 9))
						|| (ChessOracle::GetPieceOnSquare(state, destination) == Piece::blank && (destination == start - 8 || (destination == start - 16 && start / 8 == 6))));
				break;
			case knight:
				legalSoFar &= legalSoFar && (destination == start + 15 || destination == start + 17 || destination == start - 15 || destination == start - 17
					|| destination == start + 10 || destination == start + 6 || destination == start - 10 || destination == start - 6);
				break;
			case bishop:
				legalSoFar &= absDiff / 8 == absDiff % 8;
				break;
			case rook:
				legalSoFar &= absDiff / 8 == 0 || absDiff % 8 == 0;
				break;
			case queen:
				legalSoFar &=
					absDiff / 8 == absDiff % 8
				|| absDiff / 8 == 0 || absDiff % 8 == 0;
				break;
			case king:
				legalSoFar &= absDiff / 8 <= 1 && absDiff % 8 <= 1;
				break;
			}

			if (!legalSoFar)
			{
				Program::Log("------------------\nIllegal move!\nPiece broke rules.\nPosition: " + Utilities::PositionToFEN(state) + "\nMove: " + Utilities::LAN(move) + "\n");
				Utilities::PrintState(state, true, true);
				Program::Log("------------------");
			}
		}
	}

	return legalSoFar;
}

void UnitTesting::PlacePiece(GameState& state, Piece piece, U64 sq)
{
	state.State1 |= (U64)(piece & 1) * sq;
	state.State2 |= (U64)((piece & 2) >> 1) * sq;
	state.State3 |= (U64)((piece & 4) >> 2) * sq;
}