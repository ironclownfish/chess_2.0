// Chess++.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <stack>
#include <fstream>
#include "Chess++.h"
#include "ChessOracle.h"
#include "Utilities.h"
#include "InterfaceLayer.h"
#include "UCI.h"
#include "AI0.h"
#include "Profiler.h"
#include "UnitTesting.h"

using namespace std::chrono;

InterfaceLayer* Program::InterfaceLayer;
AI* Program::Engine;
Profiler* Program::Profiler;
U64 Program::OptionFlags = 0;
std::chrono::steady_clock::time_point Program::ProgramStartTime;

std::ofstream fstream;
std::string logfile = "C:\\Users\\ironc\\Desktop\\log.out";
bool running = false;

int _tmain(int argc, _TCHAR* argv[])
{
	Program::ProgramStartTime = std::chrono::high_resolution_clock::now();
	std::remove(logfile.c_str());
	fstream.open(logfile);
	Program::Log("started.");
	Program::Engine = new AI0();
	Program::Profiler = new Profiler();

	char arg[1024];
	running = true;
	while (running)
	{
		std::cin.getline(arg, 1024);
		Program::ExecuteCommand(arg);
	}
	Program::Log(Program::Profiler->ProfilerInfo());
	fstream.close();
	return 0;
}

void Program::ExecuteCommand(const char* arg)
{
	Program::Log("IN: " + std::string(arg));
	if (!strcmp(arg, "uci"))
	{
		delete Program::InterfaceLayer;
		Program::InterfaceLayer = new UCI();
	}

	//Process with input layer.
	if (!strcmp(arg, "print"))
		Utilities::PrintState(Game::CurrentGame.GetState(), true);
	else if (!strcmp(arg, "legalmoves"))
		Utilities::PrintLegalMoves(Game::CurrentGame.GetState());
	else if (!strcmp(arg, "cyclemoves"))
		Program::CycleLegalMoves(Game::CurrentGame.GetState());
	else if (!strcmp(arg, "profilemovegeneration"))
		Program::ProfileMoveGeneration(Game::CurrentGame.GetState());
	else if (!strcmp(arg, "runtests"))
		UnitTesting::RunTests();
	else if (Program::InterfaceLayer)
		Program::InterfaceLayer->ProcessInput(arg);
}

void Program::CycleLegalMoves(GameState state)
{
	U64 butterfly[64];
	bool inCheck = false;
	U64 movers = 0;
	ChessOracle::GetLegalMoves(state, butterfly, inCheck, movers);
	for (U8 square = 0; square < 64; square++)
	{
		U64& destinations = butterfly[square];
		for (; destinations > 0; destinations &= destinations - 1)
		{
			U16 move = (square << 6) | Utilities::trailing0s(destinations);
			Utilities::PrintState(ChessOracle::GetMoveResultWithPromotion(state, move), true);
			std::cin.get();
		}
	}
}

void Program::ProfileMoveGeneration(GameState state)
{
	U64 butterfly[64];
	bool inCheck = false;
	U64 movers = 0;
	Program::Profiler->StartTimer("movegeneration");
	for (int i = 0; i < 100000; i++)
		ChessOracle::GetLegalMoves(state, butterfly, inCheck, movers);
	Program::Profiler->StopTimer("movegeneration");
}

void Program::Output(std::string s)
{
	std::cout << s << "\n";
	std::cout.flush();
	Log("OUT: " + s);
}

void Program::Log(std::string s)
{
	fstream << s << "\n";
	fstream.flush();
}

void Program::Quit()
{
	running = false;
}