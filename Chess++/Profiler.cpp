#include "stdafx.h"
#include <string>
#include "Utilities.h"
#include "Profiler.h"


Profiler::Profiler()
{
	
}

void Profiler::StartTimer(std::string timerName)
{
	Timers[timerName] = Utilities::Timestamp();
	TimerWeights[timerName]++;
}

void Profiler::StopTimer(std::string timerName)
{
	U64 time = Utilities::Timestamp();
	auto timer = Timers.find(timerName);
	auto weight = TimerWeights.find(timerName);

	if (timer != Timers.end() && weight != TimerWeights.end())
	{
		TimerAverages[timerName] = ((TimerWeights[timerName] - 1) * TimerAverages[timerName] + time - Timers[timerName]) / TimerWeights[timerName];
	}
}

std::string Profiler::ProfilerInfo()
{
	std::string info = "TIMER AVERAGES:\n";
	char timebuf[128];
	for (auto timerAvg : TimerAverages)
	{
		_itoa_s(timerAvg.second, timebuf, 128, 10);
		info += "    " + timerAvg.first + " : " +  timebuf + "\n";
	}
	return info;
}

Profiler::~Profiler()
{
}
