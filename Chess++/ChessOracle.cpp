#include "stdafx.h"
#include "Utilities.h"
#include "Chess++.h"
#include "ChessOracle.h"

//TODO Can this be rearranged such that it can be divided up into the smallest (memory-wise) scope blocks possible?

void ChessOracle::GetLegalMoves(GameState& state, U64* butterfly, bool& inCheck, U64& movers)
{
	memset(butterfly, 0ULL, 8 * 64);
	inCheck = false;
	movers = 0;

	const U64 blankSqrs = EmptySqrs(state);
	const U64 enemyPieces = ~blankSqrs & (state.State0 ^ ~(((U64)state.Meta & 1) - 1)); //~(((U64)state.Meta & 1) - 1) === Fill(opposite color from mover) //TODO Just use Utilities::Fill()
	
	const U64 kingMask = Kings(state) & ~enemyPieces;
	const U64 kingOccNS = NSOccRay(kingMask, state);
	const U64 kingOccEW = EWOccRay(kingMask, state);
	const U64 kingOccDL = DLOccRay(kingMask, state);
	const U64 kingOccDR = DROccRay(kingMask, state);

	const U64 enPassantDestination = (state.State0 & blankSqrs);

	const U64 enemyCardinalSliders = RooksAndQueens(state) & enemyPieces;
	const U64 enemyDiagonalSliders = BishopsAndQueens(state) & enemyPieces;

	U64 enemyNSPseudo = 0;
	U64 enemyEWPseudo = 0;
#pragma warning (disable : 4146)
	for (U64 sliders = enemyCardinalSliders; sliders > 0; sliders &= sliders - 1)
	{
		const U64 slider = sliders & -sliders;
		enemyNSPseudo |= NSOccRay(slider, state);
		enemyEWPseudo |= EWOccRay(slider, state);
	}

	U64 enemyDLPseudo = 0;
	U64 enemyDRPseudo = 0;
	for (U64 sliders = enemyDiagonalSliders; sliders > 0; sliders &= sliders - 1)
	{
		const U64 slider = sliders & -sliders;
		enemyDLPseudo |= DLOccRay(slider, state);
		enemyDRPseudo |= DROccRay(slider, state);
	}
#pragma warning (restore : 4146)

	//Sliding check paths and pinned pieces (friendly and enemy).
	//Need to cast separately from pseudo-attacks because sliding check threats are occluded by king, whereas pseudo-attacks are not.
	const U64 slidingCheckThreatNS = ((enemyNSPseudo | enemyCardinalSliders) & kingOccNS); 
	const U64 slidingCheckThreatEW = ((enemyEWPseudo | enemyCardinalSliders) & kingOccEW);
	const U64 slidingCheckThreatDL = ((enemyDLPseudo | enemyDiagonalSliders) & kingOccDL);
	const U64 slidingCheckThreatDR = ((enemyDRPseudo | enemyDiagonalSliders) & kingOccDR);

	U64 checkBlockingPositions = ALL_SQRS;
	/*Generate king moves.*/
	{
		const U64 enemyKnights = Knights(state) & enemyPieces;

		//King can move to non pseudo-threatened squares.
		const U64 enemyKnightsPseudoAttacks = KnightReachSquaresParallel_Mask(enemyKnights);
		const U64 enemyPawnPseudoAttacks = (state.Meta & 1) ? WPawnAttackSquaresParallel_Mask(WPawns(state)) : BPawnAttackSquaresParallel_Mask(BPawns(state));

		const U64 enemyPseudoThreats =
			KnightReachSquaresParallel_Mask(enemyKnights)
			| SqsAndSurroundingSqsParallel_Mask(Kings(state) & enemyPieces)
			| enemyNSPseudo
			| enemyEWPseudo
			| enemyDLPseudo
			| enemyDRPseudo
			| enemyPawnPseudoAttacks;

		butterfly[Utilities::trailing0s(kingMask)] =
			SqsAndSurroundingSqsParallel_Mask(kingMask)
			& (blankSqrs | enemyPieces)
			& ~enemyPseudoThreats
			//King can't move to a square that is only safe because he is occluding an attack on it.
			//Prevent him from moving on check axis at all.
			& ~(slidingCheckThreatNS ? kingOccNS & ~enemyCardinalSliders : 0)
			& ~(slidingCheckThreatEW ? kingOccEW & ~enemyCardinalSliders : 0)
			& ~(slidingCheckThreatDL ? kingOccDL & ~enemyDiagonalSliders : 0)
			& ~(slidingCheckThreatDR ? kingOccDR & ~enemyDiagonalSliders : 0);

		/*Check and double check.*/
		{
			inCheck = (enemyPseudoThreats & kingMask) != 0;
			U8 kingSqr = Utilities::trailing0s(kingMask);
			//Exit if in double check.
			if (inCheck && (
				((kingMask & enemyKnightsPseudoAttacks) >> kingSqr)
				+ ((kingMask & enemyNSPseudo) >> kingSqr)
				+ ((kingMask & enemyEWPseudo) >> kingSqr)
				+ ((kingMask & enemyDLPseudo) >> kingSqr)
				+ ((kingMask & enemyDRPseudo) >> kingSqr)
				+ ((kingMask & enemyPawnPseudoAttacks) >> kingSqr)
				) > 1
			)
			{
				movers = butterfly[Utilities::trailing0s(kingMask)] > 0 ? kingMask : 0;
				return;
			}

			if (inCheck) //Rare, easy to branch predict.
			{
				//Find check blocking moves.
				//Checker capture is also "blocking."
				checkBlockingPositions =
				(
					(slidingCheckThreatNS | slidingCheckThreatEW) & (blankSqrs | enemyCardinalSliders)
				|	(slidingCheckThreatDL | slidingCheckThreatDR) & (blankSqrs | enemyDiagonalSliders)
				|	(KnightReachSquaresParallel_Mask(kingMask) & enemyKnights)
				|	(((state.Meta & 1) ? BPawnAttackSquaresParallel_Mask(kingMask) : WPawnAttackSquaresParallel_Mask(kingMask)) & Pawns(state) & enemyPieces) //Pretend king is pawn.
				);
			}
			else
			{
				//Castling considered when not in check.
				const U64 reachableRooks = EWOccRay(kingMask, state) & Rooks(state);
				//Conditional's value doesn't change often. Easy to branch predict.
				if (MoverMayCastleK(state)) butterfly[Utilities::trailing0s(kingMask)] |= (kingMask << 2) & ~enemyPseudoThreats & (reachableRooks >> 1) & ~(enemyPseudoThreats << 1); //Last term ensures not castling through check.
				if (MoverMayCastleQ(state)) butterfly[Utilities::trailing0s(kingMask)] |= (kingMask >> 2) & ~enemyPseudoThreats & (reachableRooks << 2) & ~(enemyPseudoThreats >> 1);
			}
			movers = butterfly[Utilities::trailing0s(kingMask)] > 0 ? kingMask : 0;
		}
	}

	/*Generate legal moves for non-kings.*/
	{
#pragma warning (disable : 4146)
		//TODO See if faster versions of the move generators can be made by knowing no piece parallelism is needed for legal moves.
		const U64 legalNonPawnDestinations = checkBlockingPositions & (enemyPieces | blankSqrs);
		const U64 moveable = ~enemyPieces;

		U64 piece = 0;

		for (U64 pieces = Knights(state) & moveable & ~(slidingCheckThreatEW | slidingCheckThreatNS | slidingCheckThreatDL | slidingCheckThreatDR); pieces > 0; pieces &= pieces - 1)
		{
			piece = pieces & -pieces;
			butterfly[Utilities::trailing0s(piece)] = legalNonPawnDestinations & KnightReachSquaresParallel_Mask(piece);
			movers |= butterfly[Utilities::trailing0s(piece)] > 0 ? piece : 0;
		}

		for (U64 pieces = BishopsAndQueens(state) & moveable & ~(slidingCheckThreatEW | slidingCheckThreatNS); pieces > 0; pieces &= pieces - 1)
		{
			piece = pieces & -pieces;
			const U64 drOcc = DROccRay(piece, state);
			const U64 dlOcc = DLOccRay(piece, state);
			butterfly[Utilities::trailing0s(piece)] |= legalNonPawnDestinations & (drOcc | dlOcc);

			//Restrict destinations to pinned axis if pinned. Rare.
			if (piece & slidingCheckThreatDL)
				butterfly[Utilities::trailing0s(piece)] &= dlOcc;
			else if (piece & slidingCheckThreatDR)
				butterfly[Utilities::trailing0s(piece)] &= drOcc;

			movers |= butterfly[Utilities::trailing0s(piece)] > 0 ? piece : 0;
		}

		for (U64 pieces = RooksAndQueens(state) & moveable & ~(slidingCheckThreatDL | slidingCheckThreatDR); pieces > 0; pieces &= pieces - 1)
		{
			piece = pieces & -pieces;
			const U64 nsOcc = NSOccRay(piece, state);
			const U64 ewOcc = EWOccRay(piece, state);
			butterfly[Utilities::trailing0s(piece)] |= legalNonPawnDestinations & (nsOcc | ewOcc);

			//Restrict destinations to pinned axis if pinned. Rare.
			if (piece & slidingCheckThreatNS)
				butterfly[Utilities::trailing0s(piece)] &= nsOcc;
			else if (piece & slidingCheckThreatEW)
				butterfly[Utilities::trailing0s(piece)] &= ewOcc;

			movers |= butterfly[Utilities::trailing0s(piece)] > 0 ? piece : 0;
		}

		for (U64 pieces = BPawns(state) & moveable & ~slidingCheckThreatEW; pieces > 0; pieces &= pieces - 1)
		{
			piece = pieces & -pieces;
			butterfly[Utilities::trailing0s(piece)] =
				checkBlockingPositions
				&	(
					(blankSqrs & DnFrom(piece))
				|	(blankSqrs & DnFrom((blankSqrs & DnFrom(piece & RANK_7))))
				|	((enemyPieces | enPassantDestination) & DnFrom(LtFrom(piece) | RtFrom(piece)))
				);

			if (piece & slidingCheckThreatNS)
				butterfly[Utilities::trailing0s(piece)] &= CardinalSquaresVParallel_Mask(piece);
			else if (piece & slidingCheckThreatDL)
				butterfly[Utilities::trailing0s(piece)] &= DnFrom(LtFrom(piece));
			else if (piece & slidingCheckThreatDR)
				butterfly[Utilities::trailing0s(piece)] &= DnFrom(RtFrom(piece));

			movers |= butterfly[Utilities::trailing0s(piece)] > 0 ? piece : 0;
		}

		for (U64 pieces = WPawns(state) & moveable & ~slidingCheckThreatEW; pieces > 0; pieces &= pieces - 1)
		{
			piece = pieces & -pieces;
			butterfly[Utilities::trailing0s(piece)] =
				checkBlockingPositions
				&	(
					(blankSqrs & UpFrom(piece))
				|	(blankSqrs & UpFrom((blankSqrs & UpFrom(piece & RANK_2))))
				|	((enemyPieces | enPassantDestination) & UpFrom(LtFrom(piece) | RtFrom(piece)))
				);

			if (piece & slidingCheckThreatNS)
				butterfly[Utilities::trailing0s(piece)] &= CardinalSquaresVParallel_Mask(piece);
			else if (piece & slidingCheckThreatDL)
				butterfly[Utilities::trailing0s(piece)] &= UpFrom(RtFrom(piece));
			else if (piece & slidingCheckThreatDR)
				butterfly[Utilities::trailing0s(piece)] &= UpFrom(LtFrom(piece));

			movers |= butterfly[Utilities::trailing0s(piece)] > 0 ? piece : 0;
		}
#pragma warning (restore : 4146)
	}

	/*Remove en-passant if it causes horizontal discovered check.*/
	//Rare. Easy to branch predict.
	if (enPassantDestination)
	{
		const U64 captureeOccEW =
			EWOccRay(
			(
					((enPassantDestination & RANK_6) >> 8)
				|	((enPassantDestination & RANK_3) << 8)
			), state);

		//Extremely rare.
		if (kingMask & captureeOccEW)
		{
			const U64 capturer = ~enemyPieces & Pawns(state) & captureeOccEW;
			//Rare.
			if (EWOccRay(capturer, state) & enemyCardinalSliders)
				butterfly[Utilities::trailing0s(capturer)] &= ~enPassantDestination;
		}
	}
}

GameState ChessOracle::GetMoveResultWithPromotion(GameState state, const U16& move)
{
	const U8 sq1 = StartSquare(move);
	const U8 sq2 = EndSquare(move);
	const U64 sq1mask = ((U64)1) << sq1;
	const U64 sq2mask = ((U64)1) << sq2;
	const U64 enemyColoredSqrs = Utilities::Fill(state.Meta) ^ state.State0;
	U64 epDst = state.State0 & ~(state.State1 | state.State2 | state.State3); //en-passant square if available

	//Write and clear rook if castling.
	__int8 dist = (sq2 - sq1); //signed int
	if ((sq1mask & state.State1 & state.State2 & state.State3) > 0 && dist * dist == 4)
	{
		const U64 rookStartSqrCompl = 
			(state.Meta & 1) == 0 ?
			(dist < 0 ? 0xFFFFFFFFFFFFFFFE : 0xFFFFFFFFFFFFFF7F)
		:	(dist < 0 ? 0xFEFFFFFFFFFFFFFF : 0x7FFFFFFFFFFFFFFF);
		state.State0 &= rookStartSqrCompl;
		state.State1 &= rookStartSqrCompl;
		state.State2 &= rookStartSqrCompl;
		state.State3 &= rookStartSqrCompl;

		const U64 rookDst = ((sq2mask >> 1) | (sq2mask << 1)) & (D_FILE | F_FILE);
		state.State0 |= rookDst & Utilities::Fill(state.Meta);
		state.State1 |= rookDst;
		state.State3 |= rookDst;
	}

	//Clear new square
	state.State0 &= ~sq2mask;
	state.State1 &= ~sq2mask;
	state.State2 &= ~sq2mask;
	state.State3 &= ~sq2mask;

	//Write new square
	state.State0 |= (U64)(state.Meta & 1) << sq2;
	state.State1 |= ((state.State1 & sq1mask) >> sq1) << sq2;
	state.State2 |= ((state.State2 & sq1mask) >> sq1) << sq2;
	state.State3 |= ((state.State3 & sq1mask) >> sq1) << sq2;

	//Clear old square.
	state.State0 &= ~sq1mask;
	state.State1 &= ~sq1mask;
	state.State2 &= ~sq1mask;
	state.State3 &= ~sq1mask;

	//Clear en-passant victim square. (Different than destination)
	epDst &= sq2mask & (state.State1 ^ state.State2) & ~state.State3; //Stays set if en-passant actually occurred.
	state.State1 &= ~(enemyColoredSqrs & (epDst << 8)) | (state.State2 | state.State3);	//If enemy color above en-p dst, remove... unless proven not a white pawn (s2 | s3).
	state.State2 &= ~(enemyColoredSqrs & (epDst >> 8)) | (state.State1 | state.State3);

	//Clear old en-passant opportunity.
	state.State0 &= state.State1 | state.State2 | state.State3;	
	//Set new en-passant opportunity.
	{
		U64 epSqr =
			sq1mask & (RANK_2 | RANK_7)
			& ((sq2mask << 16) | (sq2mask >> 16));

		epSqr |= (epSqr << 8) | (epSqr >> 8);
		state.State0 |= epSqr & (RANK_3 | RANK_6);
	}

	//Promote
	{
		const U64 pType = move >> 14;
		const U64 promotedMask = sq2mask & ((RANK_1 & BPawns(state)) | (RANK_8 & WPawns(state)));
		state.State1 &= ~promotedMask;
		state.State2 &= ~promotedMask;

		//Promoted piece type is (queen - pType)
		state.State1 |= promotedMask & ((pType & 1) * promotedMask);				//odd pType => odd promoted type
		state.State2 |= promotedMask & ((~((pType >> 1) ^ pType) & 1) * promotedMask);	//pType 0 and 3. i.e. bit1 == bit2
		state.State3 |= promotedMask & ((~((pType >> 1) & pType) & 1) * promotedMask);	//pType 0, 1, and 2.
	}

	//Revoke castling rights if king or rook moved.
	state.Meta &= 
	~(
		(sq1mask & H_FILE) && (sq2mask & Rooks(state))	? WHITE_KINGSIDE_CASTLE >> ((state.Meta & 1) << 1) 
	:	(sq1mask & A_FILE) && (sq2mask & Rooks(state))	? WHITE_QUEENSIDE_CASTLE >> ((state.Meta & 1) << 1)
	:	(sq2mask & Kings(state))						? (WHITE_KINGSIDE_CASTLE | WHITE_QUEENSIDE_CASTLE) >> ((state.Meta & 1) << 1)
	:	(U16)0
	);

	state.Meta ^= 1;	//Next player's turn.
	state.Meta += 8;	//Increment 50 move counter. TODO shouldn't always do.



	return state;
}

Piece ChessOracle::GetPieceOnSquare(GameState state, U8 square)
{
	U8 piece = ((state.State1>>square) & 1) | (((state.State2>>square) & 1) << 1) | (((state.State3>>square) & 1) << 2);
	return (Piece)piece;
}

Piece ChessOracle::GetPieceOnSquareMask(GameState state, U64 squareMask)
{
	U8 piece = (state.State1 & squareMask) | ((state.State2 & squareMask) << 1) | ((state.State3 & squareMask) << 2) >> Utilities::log2(squareMask);
	return (Piece)piece;
}

bool ChessOracle::IsDraw(GameState state)
{	
	U16 draws = state.Meta & 0x38;
	return draws == 50 || draws == 53 || draws == 54;
}

U64 ChessOracle::KnightReachSquaresParallel_Mask(U64 sqs)
{
	U64 ud2 = (~RANK_8 & ~RANK_7 & sqs) << 16;
	ud2 |= (~RANK_1 & ~RANK_2 & sqs) >> 16;
	 U64 lr2 = (~A_FILE & ~B_FILE & sqs) >> 2;
	lr2 |= (~H_FILE & ~G_FILE & sqs) << 2;
	return LtFrom(ud2) | RtFrom(ud2) | UpFrom(lr2) | DnFrom(lr2);
}

U64 ChessOracle::WPawnAttackSquaresParallel_Mask(U64 sqs)
{
	return UpFrom(LtFrom(sqs) | RtFrom(sqs));
}

U64 ChessOracle::BPawnAttackSquaresParallel_Mask(U64 sqs)
{
	return DnFrom(LtFrom(sqs) | RtFrom(sqs));
}

U64 ChessOracle::CardinalSquaresVParallel_Mask(U64 sqs)
{
	sqs |= sqs >> 8 | sqs << 8; //TODO Can this be optimized when non-parallel in the same way as diagonals?
	sqs |= sqs >> 16 | sqs << 16;
	return sqs | sqs >> 32 | sqs << 32;
}

U64 ChessOracle::SqsAndSurroundingSqsParallel_Mask(U64 sqs)
{
	const U64 UD = UpFrom(sqs) | DnFrom(sqs) | sqs;
	return LtFrom(UD) | RtFrom(UD) | UD;
}

//Has an extra magic number raycast optimization compared to parallel version.
U64 ChessOracle::DiagonalsFromSquare(const U64 sq)
{
	return DiagonalLFromSquare(sq) | DiagonalRFromSquare(sq);
}

U64 ChessOracle::DiagonalLFromSquare(const U64 sq)
{
	return (DIAG_L >> (8 - Utilities::trailing0s((sq * DIAG_L) >> 55))) & ((HalfDR & sq) > 0 ? HalfDR : ~HalfDR);
}

U64 ChessOracle::DiagonalRFromSquare(const U64 sq)
{
	return (DIAG_R << Utilities::trailing0s(((sq * (DIAG_R + 1)) & ~H_FILE) >> 56)) & ((HalfDL & sq) > 0 ? HalfDL : ~HalfDL);
}

//Includes occluder.
U64 ChessOracle::NSOccRay(U64 sqs, const GameState& state) //TODO Make parallel.
{
	const U64 ray = CardinalSquaresVParallel_Mask(sqs) & ~sqs;

	U64 forward = ray & ~EmptySqrs(state);
	U64 reverse = Utilities::byteswap(forward);
	forward -= sqs;
	reverse -= Utilities::byteswap(sqs);
	forward ^= Utilities::byteswap(reverse);
	forward &= ray;
	return forward;
}

//Includes occluder.
U64 ChessOracle::EWOccRay(U64 sqs, const GameState& state) //TODO Make parallel. (Is, except ~sqs assumes only one caster.)
{
	U64 o = ~(EmptySqrs(state) | sqs);
	U64 leftAttacks = ((o | H_FILE) ^ ((o | H_FILE) - sqs));
	o = Utilities::MirrorH(o);
	return ~sqs & (leftAttacks | Utilities::MirrorH((o | H_FILE) ^ ((o | H_FILE) - Utilities::MirrorH(sqs))));
}

//Includes occluder.
U64 ChessOracle::DLOccRay(U64 sqs, const GameState& state) //TODO Make parallel.
{
	const U64 ray = DiagonalLFromSquare(sqs) & ~sqs;

	U64 forward = ray & ~EmptySqrs(state);
	U64 reverse = Utilities::byteswap(forward);
	forward -= sqs;
	reverse -= Utilities::byteswap(sqs);
	forward ^= Utilities::byteswap(reverse);
	forward &= ray;
	return forward;
}

//Includes occluder.
U64 ChessOracle::DROccRay(U64 sqs, const GameState& state) //TODO Make parallel.
{
	const U64 ray = DiagonalRFromSquare(sqs) & ~sqs;

	U64 forward = ray & ~EmptySqrs(state);
	U64 reverse = Utilities::byteswap(forward);
	forward -= sqs;
	reverse -= Utilities::byteswap(sqs);
	forward ^= Utilities::byteswap(reverse);
	forward &= ray;
	return forward;
}

Game Game::CurrentGame;

void Game::SetState(GameState state)
{
	State = state;
}

GameState Game::GetState()
{
	return State;
}