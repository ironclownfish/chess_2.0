#pragma once
#include "Defines.h"
#include <map>

class Profiler
{
public:
	Profiler();
	
	void StartTimer(std::string timerName);
	void StopTimer(std::string timerName);
	std::string ProfilerInfo();
	~Profiler();

private:

	std::map<std::string, U64> Timers;
	std::map<std::string, U64> TimerAverages;
	std::map<std::string, U64> TimerWeights;
};

