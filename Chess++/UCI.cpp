#include "stdafx.h"
#include "UCI.h"
#include "Chess++.h"
#include "ChessOracle.h"
#include "Utilities.h"
#include "AI.h"
#include <sstream>
#include <iterator>
#include <cctype>
#include <string>

using namespace std;

UCI::UCI()
{
	Program::Log("UCI Init");
}

void UCI::ProcessInput(const char* input)
{
	std::vector<std::string> args = Tokenize(input);
	if (args.size() == 0) return;

	if		(Is(args[0], "uci"		))			SendStartupResponse();
	else if (Is(args[0], "debug"	))			SetDebugMode(args);
	else if (Is(args[0], "isready"	))			Program::Output("readyok");
	else if (Is(args[0], "ucinewgame"))			Game::CurrentGame.SetState(ChessOracle::StartingPosition);
	else if (Is(args[0], "position"	))			SetPosition(args);
	else if (Is(args[0], "go"		))			StartSearch(args);
	else if (Is(args[0], "stop"		))			StopSearch();
	else if (Is(args[0], "quit"		))			Program::Quit();
}

void UCI::SendInfo(U8 depth, U64 time_ms, U64 nodes, vector<U16> pv, __int32 score_cp, __int8 mateIn, U8 ARG_FLAGS)
{
	string out = "info ";
	string pvline = "";
	for (int move_i = 0; move_i < depth; move_i++)
		pvline += Utilities::LAN(pv[move_i]) + " ";
	string args[] = {"depth", "time", "nodes", "pv", "score cp", "score mate"};
	string infos[] = {to_string(depth), to_string(time_ms), to_string(nodes), pvline, to_string(score_cp), to_string(mateIn)};

	for (int arg_i = 0; arg_i < 6; arg_i++)
	{
		if ((ARG_FLAGS >> arg_i) % 2 == 1)
		{
			out +=
				(arg_i == 0 ? "" : " ") +
				args[arg_i] + " " +
				infos[arg_i];
		}
	}

	Program::Output(out);
}

void UCI::AnnounceMove(U16 move)
{
	Program::Output("bestmove " + Utilities::LAN(move));
}

vector<std::string> UCI::Tokenize(const char* input)
{
	using namespace std;

	istringstream iss(input);
	vector<string> args
	{
		istream_iterator<string>{iss},
		istream_iterator<string>{}
	};

	return args;
}

bool UCI::Is(string arg, const char* test)
{
	return !strcmp(arg.c_str(), test);
}

void UCI::SendStartupResponse()
{
	Program::Output("id name " + Program::Engine->ENGINE_NAME);
	Program::Output("id author " + Program::Engine->ENGINE_AUTHOR);
	Program::Output("uciok");
}

void UCI::SetDebugMode(vector<string> args)
{
	if (args.size() < 2)
		return;

	if (Is(args[1], "on"))
		Program::OptionFlags |= Program::OptionFlag::Debug;
	else if (Is(args[1], "off"))
		Program::OptionFlags &= ~Program::OptionFlag::Debug;
}

void UCI::SetPosition(vector<string> args)
{
	if (args.size() < 2) return;

	args.erase(args.begin());

	if (Is(args[0], "startpos"))
	{
		Game::CurrentGame.SetState(ChessOracle::StartingPosition);
		args.erase(args.begin());
	}
	else
	{
		vector<string> subVector = vector<string>(args.begin() + 1, args.begin() + 7);
		Game::CurrentGame.SetState(ParseFEN(subVector));
		args.erase(args.begin(), args.begin() + 6);
	}

	if (args.size() >= 2 && Is(args[0], "moves"))
	{
		args.erase(args.begin());
		for (auto arg : args)
		{
			Game::CurrentGame.SetState(ChessOracle::GetMoveResultWithPromotion(Game::CurrentGame.GetState(), ParseLAN(arg)));
		}
	}
}

U16 UCI::ParseLAN(string lan)
{
	if (lan.length() < 4) return 0;
	
	if (lan[2] == 'x') lan.erase(2);

	U16 move = 0;
	move |= ((lan[0] - 'a') + 8 * (lan[1] - '0' - 1)) << 6;
	move |= ((lan[2] - 'a') + 8 * (lan[3] - '0' - 1));
	if (lan.length() == 5)
		move |= (Piece::queen - Utilities::PieceFromLetter(lan[4])) << 14;

	return move;
}

GameState UCI::ParseFEN(const vector<string>& fen)
{
	if (fen.size() != 6) return ChessOracle::BlankPosition;

	GameState state = ChessOracle::BlankPosition;
	int row = 0;
	int col = 0;
	char c;
	//// Piece positions.
	for (int i = 0; i < fen[0].length(); i++)
	{
		c = fen[0][i];
		if (isdigit(c))
		{
			col += atoi(&c);
		}
		else if (c == '/')
		{
			row++;
			col = 0;
		}
		else
		{	//Set color bit and three piece bits.
			Piece piece = Utilities::PieceFromLetter(c);
			U64 sqr = 8 * (7 - row) + col;
			state.State0 |= ((U64)(isupper(c) ? 0 : 1)) << sqr;
			state.State1 |= ((U64)(piece & 1)) << sqr;
			state.State2 |= ((U64)(piece & 2) >> 1) << sqr;
			state.State3 |= ((U64)(piece & 4) >> 2) << sqr;
			col++;
		}
	}

	//// Color to move.
	state.Meta |= fen[1][0] == 'w' ? 0 : 1;

	//// Castling rights.
	for (char c : fen[2])
	{
		switch (c)
		{
		case 'K': state.Meta |= 0x80000000;
		case 'k': state.Meta |= 0x40000000;
		case 'Q': state.Meta |= 0x20000000;
		case 'q': state.Meta |= 0x10000000;
		}
	}

	//// En-passant square.
	if (fen[3].length() == 2)
	{
		row = atoi(&fen[3][1]) - 1;
		col = 7 - ('h' - fen[3][0]);
		state.State0 |= ((U64)1) << (8 * row + col);
	}

	//// Halfmove clock. (stored in full moves)
	int moves = atoi(fen[4].c_str()) / 2;
	moves = moves < 64 ? moves : 63;
	state.Meta |= moves << 4;

	//// Fullmove number.
	moves = atoi(fen[5].c_str());
	//Unused for now.

	return state;
}

void UCI::StartSearch(vector<string> args)
{
	U16 depth = 255;
	if (args.size() > 1)
	{
		if (Is(args[1], "depth"))
			depth = atoi(args[2].c_str());
	}
	Program::Engine->Ponder(Game::CurrentGame, depth);
}

void UCI::StopSearch()
{
	Program::Engine->StopPondering();
}