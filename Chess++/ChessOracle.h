#include "Defines.h"

enum Piece
{
	blank = 0,
	wpawn = 1,
	bpawn = 2,
	knight = 3,
	bishop = 4,
	rook = 5,
	queen = 6,
	king = 7
};

// Bits in GameState.Meta
#define WHITE_KINGSIDE_CASTLE ((U16)0x8000)
#define WHITE_QUEENSIDE_CASTLE ((U16)0x4000)
#define BLACK_KINGSIDE_CASTLE ((U16)0x2000)
#define BLACK_QUEENSIDE_CASTLE ((U16)0x1000)

struct GameState
{
	/*
	State0
		Piece colors (0 == white)
		All blank squares are white except the en-passant square if there is one.
	State1 - State3
		Piece type.
	*/
	U64 State0;
	U64 State1;
	U64 State2;
	U64 State3;

	U64& State(U8 i)
	{
		return
			i == 0 ? State0 :
			i == 1 ? State1 :
			i == 2 ? State2 :
			State3;
	}

	/*
	1 : 1 : 1 : 1 : 2 : 6 : 2 : 1 : 1
	white can castle kingside : white can castle queenside : black can castle kingside : black can castle queenside
	: game over codes : 50 move rule : previous repetitions : non-mover offered draw : color to move

	game over codes :
	0 = game in progress
	1 = mover resigned
	2 = game drawn by agreement
	3 = game drawn by repetition
	*/

	U16 Meta;
};

struct MirroredBoard
{
	const U64 State0;
	const U64 State1;
	const U64 State2;
	const U64 State3;
};

namespace ChessOracle
{
	const GameState BlankPosition =
	{
		0, 0, 0, 0, 0
	};

	const GameState StartingPosition =
	{
		0xFFFF000000000000,
		0xD30000000000FFD3,
		0X5AFF00000000005A,
		0XBD000000000000BD,
		0xF000
	};

	/*
	move
		2 : 1 : 1 : 6 : 6
		promotion type : is promotion : is special move : start square : end square
		Returns empty board if move is illegal.
	*/

	void GetLegalMoves(GameState& state, U64* butterfly, bool& inCheck, U64& movers);

	GameState GetMoveResultWithPromotion(GameState state, const U16& move);
	inline Piece PromotionType(const U16 move) { return (Piece)((U8)Piece::queen - (move >> 14)); }
	inline bool IsPromotion(const U16 move) { return (move & 0x2000) > 0; }
	inline U8 StartSquare(const U16 move) { return (move >> 6) & 0x3F; }
	inline U8 EndSquare(const U16 move) { return move & 0x3F; }

	Piece GetPieceOnSquare(GameState state, U8 square);
	Piece GetPieceOnSquareMask(GameState state, U64 squareMask);

	bool IsDraw(GameState state);

	inline U64 KnightReachSquaresParallel_Mask(U64 sqs);
	inline U64 WPawnAttackSquaresParallel_Mask(U64 sqs);
	inline U64 BPawnAttackSquaresParallel_Mask(U64 sqs);
	inline U64 CardinalSquaresVParallel_Mask(U64 sqs);
	inline U64 SqsAndSurroundingSqsParallel_Mask(U64 sqs);
	inline U64 DiagonalsFromSquare(const U64 sq);
	inline U64 DiagonalLFromSquare(const U64 sq);
	inline U64 DiagonalRFromSquare(const U64 sq);
	inline U64 UpFrom(U64 sqrMask) { return (~RANK_8 & sqrMask) << 8; }
	inline U64 DnFrom(U64 sqrMask) { return (~RANK_1 & sqrMask) >> 8; }
	inline U64 RtFrom(U64 sqrMask) { return (~H_FILE & sqrMask) << 1; }
	inline U64 LtFrom(U64 sqrMask) { return (~A_FILE & sqrMask) >> 1; }

	inline U64 EmptySqrs(const GameState& state)	{ return ~(state.State1 | state.State2 | state.State3); }
	inline U64 Pawns(const GameState& state)		{ return (state.State1 ^ state.State2) & ~state.State3; }
	inline U64 WPawns(const GameState& state)		{ return state.State1 & ~(state.State2 | state.State3); }
	inline U64 BPawns(const GameState& state)		{ return ~state.State1 & state.State2 & ~state.State3;	}
	inline U64 Knights(const GameState& state)		{ return state.State1 & state.State2 & ~state.State3;	}
	inline U64 Bishops(const GameState& state)		{ return ~(state.State1 | state.State2) & state.State3; }
	inline U64 Rooks(const GameState& state)		{ return state.State1 & ~state.State2 & state.State3;	}
	inline U64 Queens(const GameState& state)		{ return ~state.State1 & state.State2 & state.State3;	}
	inline U64 Kings(const GameState& state)			{ return state.State1 & state.State2 & state.State3;	}
	inline U64 KingsAndRooks(const GameState& state)	{ return state.State1 & state.State3;}
	inline U64 RooksAndQueens(const GameState& state)	{ return (state.State1 ^ state.State2) & state.State3; }
	inline U64 BishopsAndQueens(const GameState& state)	{ return ~state.State1 & state.State3; }

	inline bool MoverMayCastleK(const GameState& state) { return (((WHITE_KINGSIDE_CASTLE >> ((state.Meta & 1) << 1))) & state.Meta) != 0; }
	inline bool MoverMayCastleQ(const GameState& state) { return (((WHITE_QUEENSIDE_CASTLE >> ((state.Meta & 1) << 1))) & state.Meta) != 0; }
	inline bool ColorMayCastleK(const GameState& state, U8 color) { return ((1ULL << (15 - (color << 1))) & state.Meta) != 0; }
	inline bool ColorMayCastleQ(const GameState& state, U8 color) { return ((1ULL << (14 - (color << 1))) & state.Meta) != 0; }
	inline bool NobodyMayCastle(const GameState& state) { return (state.Meta & 0xF000) == 0; }
	inline U64 EnPassantTarget(const GameState& state) { return ChessOracle::EmptySqrs(state) & state.State0; }
	inline U8 MovesSinceProgress(const GameState& state) { return (state.Meta >> 4) & 0x3F; }
	inline void SetPromotionBit(U16& move, GameState& state) { move |= ((RANK_8 | RANK_1) & (1ULL << EndSquare(move))) && ((1ULL << StartSquare(move)) & ChessOracle::Pawns(state)) > 0 ? 0x2000 : 0; }

	inline U64 NSOccRay(U64 sqs, const GameState& state);
	inline U64 EWOccRay(U64 sqs, const GameState& state);
	inline U64 DLOccRay(U64 sqs, const GameState& state);
	inline U64 DROccRay(U64 sqs, const GameState& state);
}

class Game
{
public:
	static Game CurrentGame;
	void SetState(GameState state);
	GameState GetState();

private:
	GameState State;
};