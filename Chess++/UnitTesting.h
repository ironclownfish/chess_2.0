#pragma once
#include "Defines.h"

namespace UnitTesting
{
	bool RunTests();
	bool RunIsolatedPieceTests();

	bool VerifyLegalMoves(GameState& state, U64* butterfly);
	void PlacePiece(GameState& state, Piece piece, U64 sq);
};