#pragma once
#include <string>
#include "InterfaceLayer.h"

struct GameState;

class UCI : public InterfaceLayer
{
public:

	UCI();

	virtual void ProcessInput(const char* input) override;
	virtual void SendInfo(U8 depth, U64 time_ms, U64 nodes, std::vector<U16> pv, __int32 score_cp, __int8 mateIn, U8 ARG_FLAGS) override;
	virtual void AnnounceMove(U16 move) override;

private:

	std::vector<std::string> Tokenize(const char* input);
	bool Is(std::string arg, const char* test);
	void SendStartupResponse();
	void SetDebugMode(std::vector<std::string> args);
	void SetPosition(std::vector<std::string> args);
	void StartSearch(std::vector<std::string> args);
	void StopSearch();
	GameState ParseFEN(const std::vector<std::string>& fen);
	U16 ParseLAN(std::string lan);
};

